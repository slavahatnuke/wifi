#!/usr/bin/env node
var program = require('commander');

var app = require('./server/config/application');

function next(err) {
    if (err) {
        console.error(err);
        return process.exit(1);
    }

    process.exit(0);
}

program
    .version(require('./package.json').version)
    .command('add.user [email] [password]')
    .action(function (email, password) {
        var User = app.get('User');

        new User({email: email, 'local.password': password}).save(function (err, user) {
            if (err) return next(err);
            console.log('User was created: _id: ' + user._id);
            next();
        });
    });

program
    .version(require('./package.json').version)
    .command('add.server [name] [ip]')
    .action(function (name, ip) {
        var Server = app.get('Server');

        new Server({name: name, ip: ip}).save(function (err, server) {
            if (err) return next(err);
            console.log('Server was created: _id: ' + server._id);
            next();
        });
    });

if (!process.argv.slice(2).length) {
    program.outputHelp();
}

program.parse(process.argv);