var ObjectId = require('pow-mongodb-fixtures').createObjectId;

exports.servers = [
    {
        "_id": ObjectId("566863b90383a43814db6a64"),
        "name": "Server 1",
        "ip": "127.0.0.1",
        "__v": 0
    },
    {
        "_id": ObjectId("566863b90383a43814db6a65"),
        "name": "Server 2",
        "ip": "127.0.0.1",
        "__v": 0
    }
];

