var ObjectId = require('pow-mongodb-fixtures').createObjectId;

exports.users = [
    {
        "_id": ObjectId("566809fdc6d985f507f02ba4"),
        "email": "test@test",
        "local": {
            "passwordHashed": "3e6de8785efff02e7fe4ef7a6eb58cbcc0229b18b2f2440b82232873f93d3f57",
            "passwordSalt": "289dcdd8647b5e722384c3e58283274ac1810cfdea413602074f6c21a8ff2247"
        },
        "name": "test"
    }
];