var ObjectId = require('pow-mongodb-fixtures').createObjectId;

exports.routers = [
    {
        "_id": ObjectId("566863aa0383a43814db6a63"),
        "name": "Router 1",
        "ip": "127.0.0.1",
        "login": "root",
        "password": "root",
        "servers": [ObjectId("566863b90383a43814db6a64")],
        "users": [ObjectId("566809fdc6d985f507f02ba4")],
        "port": 2010,
        "__v": 1
    },
    {
        "_id": ObjectId("566863aa0383a43814db6a64"),
        "name": "Router 2",
        "ip": "127.0.0.1",
        "login": "root",
        "password": "root",
        "servers": [ObjectId("566863b90383a43814db6a64")],
        "users": [ObjectId("566809fdc6d985f507f02ba4")],
        "port": 2011,
        "__v": 1
    },
    {
        "_id": ObjectId("566863aa0383a43814db6a65"),
        "name": "Router 3",
        "ip": "127.0.0.1",
        "login": "root",
        "password": "root",
        "servers": [ObjectId("566863b90383a43814db6a65")],
        "users": [ObjectId("566809fdc6d985f507f02ba4")],
        "port": 2012,
        "__v": 1
    }
];

