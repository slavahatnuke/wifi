/* =================================================================================
 * @author Slava Hatnuke
 * =================================================================================
 * Copyright (c) 2015 Rakuten Marketing
 * Licensed under MIT (https://github.com/linkshare/plus.garden/blob/master/LICENSE)
 * ============================================================================== */

 module.exports = function (container) {
  container.register('MongoFixtureLoaderModule', require('plus.garden.fixtures-mongo'));
  container.register('DockerComposeFixturesModule', require('plus.garden.fixtures.docker-compose'));
}