# WiFi project

## How to start
- `vagrant up` # up vagrant development environtment
- `vagrant provision` # install project env
- `vagrant ssh` # connect to virtual server by ssh
- `cd /home/vagrant/project` # go to inside of work dir
- `cd /home/vagrant/project/garden && garden.js fixtures.load` ## it will load fixtures and configure virtual OpenWRT boxes/routers (docker based)
- `cd /home/vagrant/project && npm start` # it should install `bower` and `npm` dependencies and start project
- open in your browser UI [http://192.168.10.10:3000/](http://192.168.10.10:3000/) login `test@test` password `test` - here you can click `My Routers` and setup access point and password for specific router. it will update OpenWRT box with these credentials via `SSH` and `uci`



