angular
    .module('RoutersWifi')

    .factory('LoginResource', function ($resource) {
        return $resource('/api/login');
    })

    .factory('LogoutResource', function ($resource) {
        return $resource('/api/logout');
    })

    .factory('User', function ($resource) {
        return $resource('/api/users/:userId', {userId: '@_id'});
    })
    .factory('Me', function ($resource) {
        return $resource('/api/me');
    })

    .factory('UserService', function ($q, User, Me, LogoutResource) {
        var self = {

            user: null,

            getUser: function () {
                return this.user;
            },
            getUserId: function () {
                if (this.user && this.user._id) return this.user._id || null;
            },
            getUsers: function () {
                return User.query();
            },
            load: function () {
                return $q(function (resolve, reject) {
                    self.user = Me.get(function () {
                        resolve(self.user);
                    }, function (err) {
                        reject(err);
                    });
                });
            },
            logout: function () {
                return $q(function (resolve, reject) {
                    LogoutResource.save(function () {
                        self.user = null;
                        resolve();
                    }, reject);
                });

            }
        };

        self.load();

        return self;
    })

    .controller('LoginCtrl', function ($scope, $state, LoginResource, UserService) {
        $scope.login = function () {

            LoginResource.save({
                username: $scope.loginName,
                password: $scope.userPassword
            }, function () {
                UserService.load().then(function () {
                    $state.go('app.routers');
                });

            })

        }

    })

    .controller('LogoutCtrl', function ($scope, LogoutResource, $state, UserService) {
        UserService.logout().then(function () {
            $state.go('app.login');
        });
    })
;


