angular
    .module('resource', ['ngResource'])
    .config(function ($resourceProvider) {
        $resourceProvider.defaults.actions.update = {method: 'PUT'};
    })
    .factory('resource', function ($resource) {
        return function () {
            var aResource = $resource.apply($resource, arguments);

            aResource.prototype.save = function () {
                if (this._id) {
                    return this.$update.apply(this, arguments);
                } else {
                    return this.$save.apply(this, arguments);
                }
            };

            return aResource;
        }
    });