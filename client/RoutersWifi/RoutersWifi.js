angular
    .module('RoutersWifi', ['ui.router', 'resource', 'ui.select', 'ngSanitize', 'angular-loading-bar'])
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.includeBar = true;

        $httpProvider.interceptors.push('HttpInterceptor');

        $urlRouterProvider.otherwise("/app/login");

        $stateProvider
            .state('app', {
                url: "/app",
                templateUrl: "RoutersWifi/RoutersWifi.html",
                controller: function ($scope, UserService) {
                    $scope.UserService = UserService;
                    $scope.$watch('UserService.user._id', function (id) {
                        if (id) {
                            $scope.user = $scope.user || UserService.getUser();
                        } else {
                            $scope.user = null;
                        }
                    });
                }
            })
            .state('app.login', {
                url: "/login",
                templateUrl: "RoutersWifi/auth/login.html",
                controller: "LoginCtrl"
            })
            .state('app.logout', {
                url: "/logout",
                controller: "LogoutCtrl"
            })
            .state('app.me', {
                url: "/me"
            })
            .state('app.me.routers', {
                url: "/routers",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/me/routers/routers.html",
                        controller: "MeRoutersCtrl"
                    }
                }
            })
            .state('app.me.routers.show', {
                url: "/:routerId",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/me/routers/router.html",
                        controller: "MeRouterDetailsCtrl"
                    }
                }
            })
            .state('app.me.routers.setup', {
                url: "/:routerId/setup",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/me/routers/router-setup.html",
                        controller: "MeRouterSetupCtrl"
                    }
                }
            })

            .state('app.routers', {
                url: "/routers",
                templateUrl: "RoutersWifi/routers/routers.html",
                controller: "RoutersCtrl"
            })
            .state('app.routers.new', {
                url: "/new",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/routers/router-form.html",
                        controller: "RouterFormCtrl"
                    }
                }
            })
            .state('app.routers.edit', {
                url: "/:routerId/edit",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/routers/router-form.html",
                        controller: "RouterFormCtrl"
                    }
                }
            })
            .state('app.routers.show', {
                url: "/:routerId",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/routers/router.html",
                        controller: "RouterDetailsCtrl"
                    }
                }
            })
            .state('app.servers', {
                url: "/servers",
                templateUrl: "RoutersWifi/servers/servers.html",
                controller: "ServersCtrl"
            })
            .state('app.servers.new', {
                url: "/new",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/servers/server-form.html",
                        controller: "ServerFormCtrl"
                    }
                }
            })
            .state('app.servers.edit', {
                url: "/:serverId/edit",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/servers/server-form.html",
                        controller: "ServerFormCtrl"
                    }
                }
            })
            .state('app.servers.show', {
                url: "/:serverId",
                views: {
                    '@app': {
                        templateUrl: "RoutersWifi/servers/server.html",
                        controller: "ServerDetailsCtrl"
                    }
                }
            })
        ;

    })

    .factory('HttpInterceptor', function ($q, $injector) {
        return {
            'responseError': function (rejection) {
                if (rejection.status == 401) {
                    $injector.get('$state').go('app.login')
                }
                return $q.reject(rejection);
            }
        };
    })
;


