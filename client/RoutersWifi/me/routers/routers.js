angular
    .module('RoutersWifi')

    .factory('MeRouter', function (resource) {
        return resource('/api/me/routers/:routerId', {routerId: '@_id'});
    })

    .controller('MeRoutersCtrl', function ($scope, MeRouter) {

        MeRouter.query({}, function (routers) {
            $scope.routers = routers;
        });
    })

    .controller('MeRouterDetailsCtrl', function ($scope, MeRouter, $stateParams) {

        if ($stateParams.routerId) {
            MeRouter.get({routerId: $stateParams.routerId}, function (router) {
                $scope.router = router;
            });
        }
    })

    .controller('MeRouterSetupCtrl', function ($scope, UserRouter, $stateParams, $state) {

        $scope.init = function () {
            if ($stateParams.routerId) {
                UserRouter.get({routerId: $stateParams.routerId}, function (router) {
                    $scope.router = router;
                });

            } else {
                $scope.router = new UserRouter({
                    settings: {
                        login: "",
                        password: ""
                    }
                });
            }
        };

        $scope.init();

        $scope.save = function () {
            $scope.router.save(function () {
                $state.go('app.me.routers.show', {routerId: $scope.router._id});
            });
        };
    })


;


