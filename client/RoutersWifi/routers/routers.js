angular
    .module('RoutersWifi')

    .factory('Router', function (resource) {
        return resource('/api/routers/:routerId', {routerId: '@_id'});
    })

    .factory('UserRouter', function (resource) {
        return resource('/api/me/routers/:routerId', {routerId: '@_id'});
    })

    .controller('RoutersCtrl', function ($scope, Router) {

        $scope.getRouters = function () {
            Router.query({}, function (routers) {
                $scope.routers = routers;
            });
        };

        $scope.getRouters();
    })

    .controller('RouterFormCtrl', function ($scope, Router, $state, $stateParams, UserService, Server) {

        $scope.init = function () {
            if ($stateParams.routerId) {
                Router.get({routerId: $stateParams.routerId}, function (router) {
                    $scope.router = router;
                });

            } else {
                $scope.router = new Router({
                    name: "",
                    ip: "",
                    login: "",
                    password: "",
                    port: 22,
                    users: [],
                    servers: []
                });
            }

            $scope.servers = Server.query();

            $scope.users = UserService.getUsers();
        };

        $scope.init();

        $scope.save = function () {
            $scope.router.save(function () {
                $state.go('app.routers.show', {routerId: $scope.router._id});
            });
        };
    })

    .controller('RouterDetailsCtrl', function ($scope, Router, $state, $stateParams) {

        if ($stateParams.routerId) {
            Router.get({routerId: $stateParams.routerId}, function (router) {
                $scope.router = router;
            });
        }

        $scope.delete = function (router) {
            if ($stateParams.routerId == router._id) {
                router.$delete().then(function () {
                    $state.go('app.routers');
                });
            }
        };
    })

;


