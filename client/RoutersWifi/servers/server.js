angular
    .module('RoutersWifi')

    .factory('Server', function (resource) {
        return resource('/api/servers/:serverId', {serverId: '@_id'});
    })

    .controller('ServersCtrl', function ($scope, Server) {

        $scope.getServers = function () {
            Server.query({}, function (servers) {
                $scope.servers = servers;
            });
        };

        $scope.getServers();
    })

    .controller('ServerFormCtrl', function ($scope, $state, $stateParams, Server) {

        $scope.init = function () {
            if ($stateParams.serverId) {
                Server.get({serverId: $stateParams.serverId}, function (server) {
                    $scope.server = server;
                });

            } else {
                $scope.server = new Server({
                    name: "",
                    ip: ""
                });
            }
        };

        $scope.init();

        $scope.save = function () {
            $scope.server.save().then(function () {
                $state.go('app.servers.show', {serverId: $scope.server._id});
            });
        };
    })

    .controller('ServerDetailsCtrl', function ($scope, $stateParams, $state, Server) {
        if ($stateParams.serverId) {
            Server.get({serverId: $stateParams.serverId}, function (server) {
                $scope.server = server;
            });
        }

        $scope.delete = function () {
            $scope.server.$delete().then(function () {
                $state.go('app.servers');
            });
        };
    })


;


