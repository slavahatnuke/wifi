module.exports = function (User) {

    var passport = require('passport');
    var LocalStrategy = require('passport-local').Strategy;


    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, '-local.passwordHashed -local.passwordSalt', done);
    });

    passport.use(new LocalStrategy({
            passReqToCallback: true
        },
        function (req, email, password, done) {
            User.findOne({'email': email}, function (err, user) {
                if (err) {
                    return done(err);
                }

                if (!user || !user.validPassword(password)) {
                    return done(null, false);
                }

                return done(null, user);
            });
        }
    ));

    return passport;
};