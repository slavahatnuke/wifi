var RouterService = function (SshService) {
    
    this.setup = function (router, next) {
        
        var data = {
            AP: router.settings.accessPoint,
            password: router.settings.password
        };

        ////var string = JSON.stringify(data).replace(/"/gm, '\\"');


        var commands = [
            'uci set wireless.@wifi-iface[0].ssid=' + data.AP,
            'uci set wireless.@wifi-iface[0].encryption=psk2',
            'uci set wireless.@wifi-iface[0].key=' + data.password,
            'uci commit wireless',
            '/etc/init.d/network restart'
        ];

        var connection = SshService
            .createConnection(router.ip, router.port, router.login, router.password);

        SshService.execList(connection, commands, function (err) {
            if (err) return next(err);

            next(null, router);
        });
    }
    
};

module.exports = RouterService;