module.exports = {

    ssh: {
        readyTimeout: 2000
    },
    "app": {
        "port": 3000
    },
    "session": {
        "secret": "wifi$app$$secrettttt"
    },
    "host": {
        "url": "http://localhost:3000"
    },
    "mongo": {
        "uri": "mongodb://localhost/wifi",
        "debug": false
    }
};