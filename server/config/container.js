module.exports = function (container) {
    container.register('SshService', require('../ssh/SshService'), ['config']);
    container.register('RouterService', require('../services/RouterService'), ['SshService']);
    container.register('Mongoose', require('../services/Mongoose'), ['config/mongo']);
    container.register('Passport', require('../services/Passport'), ['User']);

    container.register('User', require('../models/user'), ['Mongoose']);
    container.register('Router', require('../models/router'), ['Mongoose']);
    container.register('Server', require('../models/server'), ['Mongoose']);
};