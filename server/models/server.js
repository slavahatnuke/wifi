module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var ServerSchema = new Schema({
        name: String,
        ip: String
    });

    return mongoose.model('Server', ServerSchema);

};

