module.exports = function (mongoose) {

    var Schema = mongoose.Schema;
    var crypto = require("crypto");

    var UserSchema = new Schema({

        email: String,
        local: {
            name: {type: String, index: true},
            passwordSalt: String,
            passwordHashed: String
        },
        role: String
    });

    UserSchema.set('toJSON', {getters: true, virtuals: true});

    function hash(data) {
        return crypto
            .createHash("sha256")
            .update(data)
            .digest("hex");
    }

    UserSchema.methods = {

        validPassword: function (password) {
            return this.local.passwordHashed === hash(this.local.passwordSalt + password);
        },
        setPassword: function (password) {
            var salt = 'key-' + Math.random() + ' ' + Math.random() + new Date();
            this.local.passwordSalt = hash(salt);
            this.local.passwordHashed = hash(this.local.passwordSalt + password);

        }

    };


    UserSchema.virtual('local.password').set(function (password) {
        this.setPassword(password);
    });


    return mongoose.model('User', UserSchema);

};

