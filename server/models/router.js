module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var RouterSchema = new Schema({

        name: String,
        ip: String,
        login: String,
        password: String,
        port: {type: Number, default: 22},
        users: [{type: Schema.Types.ObjectId, ref: "User", default: []}],
        servers: [{type: Schema.Types.ObjectId, ref: "Server", default: []}],
        settings: {
            accessPoint: String,
            password: String
        }
    });

    return mongoose.model('Router', RouterSchema);

};

