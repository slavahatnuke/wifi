module.exports = function (app) {

    var _ = require('lodash');
    var Router = app.container.get('Router');
    var RouterService = app.container.get('RouterService');

    var form = require("express-form"),
        field = form.field;

    var RouterForm = form(
        field("name").required().trim(),
        field("ip").trim(),
        field("login").trim(),
        field("password").required(),
        field("port").trim().isNumeric(),
        field("users").array(),
        field("servers").array()
    );

    var MeRouterForm = form(
        field("settings.accessPoint").required().trim(),
        field("settings.password").required()
    );

    app.param('routerId', function (req, res, next, routerId) {
        Router.findById(routerId, function (err, router) {
            if (err) return next(err);

            if (!router) {
                res.sendStatus(404);
            }
            else {
                req.Router = router;
            }
            next();
        });

    });

    app.get('/api/me/routers', function (req, res, next) {

        Router.find({users: req.user}, 'name ip settings', function (err, routers) {
            if (err) return next(err);
            res.json(routers);
        });

    });

    app.get('/api/me/routers/:routerId', function (req, res) {
        res.json(req.Router);
    });

    app.put('/api/me/routers/:routerId', MeRouterForm, function (req, res, next) {
        if (req.form.isValid) {
            _.assign(req.Router, req.form);

            req.Router.save(function (err, router) {
                if (err) return next(err);

                RouterService.setup(router, function (err, router) {
                    if (err) return next(err);
                    res.json(router);
                });
            });
        }
        else {
            res.sendStatus(400);
        }

    });

    app.get('/api/routers', function (req, res, next) {
        Router.find({}, function (err, routers) {
            if (err) return next(err);
            res.json(routers);
        });
    });

    app.get('/api/routers/:routerId', function (req, res) {
        res.json(req.Router);
    });

    app.post('/api/routers', RouterForm, function (req, res, next) {
        if (req.form.isValid) {
            var router = new Router(req.form);

            router.save(function (err, router) {
                if (err) return next(err);

                return res.json(router);
            });
        }
        else {
            return res.status(400).json(req.form.errors);
        }
    });

    app.delete('/api/routers/:routerId', function (req, res, next) {
        req.Router.remove(function (err) {
            if (err) return next(err);
            res.sendStatus(200);
        });
    });

    app.put('/api/routers/:routerId', RouterForm, function (req, res, next) {
        if (req.form.isValid) {
            _.assign(req.Router, req.form);

            req.Router.save(function (err, router) {
                if (err) return next(err);
                res.json(router);
            });
        }
        else {
            res.sendStatus(400);
        }
    });
};