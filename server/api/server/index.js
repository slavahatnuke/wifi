module.exports = function (app) {

    var _ = require('lodash');
    var Server = app.container.get('Server');

    var form = require("express-form"),
        field = form.field;

    var ServerForm = form(
        field("name").required().trim(),
        field("ip").trim()
    );

    app.param('serverId', function (req, res, next, serverId) {
        Server
            .findById(serverId, function (err, server) {
                if (err) return next(err);

                if (!server) {
                    res.sendStatus(404);
                }
                else {
                    req.Server = server;
                }
                next();
            });
    });

    app.get('/api/servers', function (req, res, next) {
        Server.find({}, function (err, servers) {
            if (err) return next(err);
            res.json(servers);
        });
    });

    app.post('/api/servers', ServerForm, function (req, res, next) {
        if (req.form.isValid) {
            var server = new Server(req.form);

            server.save(function (err, server) {
                if (err) return next(err);

                return res.json(server);

            });
        }
        else {
            return res.status(400).json(req.form.errors);
        }
    });

    app.get('/api/servers/:serverId', function (req, res) {
        res.status(200).send(req.Server);
    });


    app.delete('/api/servers/:serverId', function (req, res, next) {
        req.Server.remove(function (err) {
            if (err) {
                return next(err);
            }

            res.sendStatus(200);
        });
    });

    app.put('/api/servers/:serverId', ServerForm, function (req, res, next) {
        if (req.form.isValid) {
            _.assign(req.Server, req.form);

            req.Server.save(function (err, server) {
                if (err) return next(err);
                res.json(server);
            });
        }
    });
};