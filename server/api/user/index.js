module.exports = function (app) {

    var User = app.container.get('User');

    app.get('/api/users', function (req, res, next) {
        User.find({}, '-local', function (err, users) {
            if (err) return next(err);
            res.json(users);
        });
    });

};