module.exports = function (app) {
    var Passport = app.container.get('Passport');
    //

    app.post('/api/login',
        Passport.authenticate('local', { successRedirect: '/api/me'})
    );

    app.post('/api/logout', function (req, res) {
        req.logout();
        res.status(200).send();
    });


    app.use('/api', function (req, res, next) {
        if (!req.user) {
            return res.status(401).send();
        }
        next();
    });

    app.get('/api/me', function (req, res) {
        res.json(req.user);
    });

}
;