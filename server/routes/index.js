module.exports = function (app) {

    require('../api/auth')(app);
    require('../api/routers')(app);
    require('../api/user')(app);
    require('../api/server')(app);

};