var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var app = express();
require('./config/application').wrap(app);

//var SshService = app.application.get('SshService');
//
//SshService.exec('cd / && ls && echo "HELLO WRT"', function (err, result) {
//    if (err) return console.log(err);
//
//    console.log('>>', result);
//});

app.container.get('Mongoose');
var Passport = app.container.get('Passport');

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var MongoSessionStore = require('connect-mongo')(session);

app.use(session({
    secret: app.config.get('session.secret'),
    cookie: { maxAge: 4*7*24*60*60*1000 }, // 4 weeks
    resave: true,
    saveUninitialized: true,
    rolling: true,
    store: new MongoSessionStore({ url: app.config.get('mongo.uri') })
}));



app.use(Passport.initialize());
app.use(Passport.session());


app.use(express.static(__dirname + './../client'));
require('./routes')(app);

module.exports = app;
