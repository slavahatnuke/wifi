var SshService = function (config) {
    var SshClient = require('ssh2').Client;

    var async = require('async');
    var Connection = function (host, port, user, password) {
        var self = this;

        this.client = null;

        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;


        this.getClient = function (next) {

            self.client = new SshClient();

            self.client
                .on('ready', function () {
                    next(null, self.client);
                }).on('error', function(err) {
                    next(err)
                }).connect(
                {
                    host: self.host,
                    port: self.port,
                    username: self.user,
                    password: self.password,
                    readyTimeout: config.get('ssh.readyTimeout')
                }
            );
        }
    };

    this.createConnection = function (host, port, user, password) {
        return new Connection(host, port, user, password)
    };

    this.execList = function (connection, commands, next) {
        connection.getClient(function (err, client) {
            if (err) return next(err);

            async.eachSeries(commands, function (command, next) {
                call(client, command, next);
            }, function (err, result) {
                client.end();
                next(err, result);
            });
        });
    };

    var call = function (client, command, next) {
        var output = [];

        client.exec(command, function (err, stream) {
            if (err) return next(err);

            stream.on('close', function (code) {

                if (code) {
                    return next(new Error('Exited with code: ' + code));
                }

                next(null, output.join(''));

            }).on('data', function (aData) {
                output.push(aData.toString())
            }).stderr.on('data', function (data) {
                    //console.log('STDERR: ' + data);
                });
        });
    };

    this.exec = function (connection, command, next) {

        connection.getClient(function (err, client) {
            if (err) return next(err);
            call(client, command, function (err, result) {
                client.end();
                next(err, result);
            });
        });
    }
};

module.exports = SshService;