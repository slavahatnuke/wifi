var app = require('./server/index');

var server = app.listen(app.config.get('app.port'), function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Listening at http://%s:%s env %s', host, port, app.config.get('env'));
});